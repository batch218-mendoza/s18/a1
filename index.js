// console.log("Hello World!");

function Add2Numbers(num1, num2){
	let sumOf2Numbers = num1 + num2;
	console.log("Displayed sum of " +num1+ " and " +num2);
	console.log(sumOf2Numbers);
}

Add2Numbers(5,15);

console.log("----------------------------");


function subtract2Numbers(num1, num2){
	let differenceOf2Numbers = num1 - num2;
	console.log("Displayed difference of " +num1+ " and " +num2);
	console.log(differenceOf2Numbers);
}

subtract2Numbers(20,5);


console.log("----------------------------");


function returnMultiply2Numbers(num1,num2){
	numbers = num1 * num2;
	console.log("Displayed product of " +num1+ " and " +num2);
	return numbers;

}

let product = returnMultiply2Numbers(50,10);
console.log(product);



console.log("----------------------------");


function returnDivide2Numbers(num1,num2){
	numbers = num1 / num2;
	console.log("Displayed quotient of " +num1+ " and " +num2);
	return numbers;

}

let quotient = returnDivide2Numbers(50,10);
console.log(quotient);


console.log("----------------------------");

function getArea(radius){
	let	pi = 3.14159265359;
	let	area = pi * (radius**2);
	console.log("The result of getting the area of a circle with " +radius+ " radius:");
	return area;
}

let circleArea = getArea(15);
console.log(circleArea);

console.log("----------------------------");

function getAverage(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4) / 4;
	console.log("The average of " +num1+ ", " +num2+ ", " +num3+ " and " +num4+ ":");
	return average;
}

let averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar);


console.log("----------------------------");

console.log("Passing Grade is 75% and up");
function ifPassedTest(score,tScore){
	let checkScore = (score / tScore) *100;
	let isPassed = checkScore >= 75;
	console.log("is " +score+ "/" +tScore+ " a passing score");
	console.log("your percentage is " + checkScore + "%");
	return isPassed;

}

let isPassingScore = ifPassedTest(35,50);
console.log(isPassingScore);
